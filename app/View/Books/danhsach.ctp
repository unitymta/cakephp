<?php
echo $this->Paginator->prev('Previous ', null, null, array('class' => 'disabled'));
echo " | " . $this->Paginator->numbers() . " | ";
echo $this->Paginator->next(' Next', null, null, array('class' => 'disabled'));
echo " Page " . $this->Paginator->counter();
?>

<?php
if ($data == NULL) {
	echo "Data empty";
} else {
	echo "<table>
	<tr>
		<td>Id</td>
		<td>Title</td>
		<td>Description</td>
	</tr>";

	foreach ($data as $item) {
		echo "<tr>";
		echo "<td>" . $item['Book']['id'] . "</td>";
		echo "<td>" . $item['Book']['title'] . "</td>";
		echo "<td>" . $item['Book']['description'] . "</td>";
		echo "</tr>";
	}
}
?>
