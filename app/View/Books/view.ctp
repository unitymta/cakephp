<?php echo $this->Form->create('Book', array('url' => array('controller' => 'books', 'action' => 'search'), 'type' => 'post', 'id' => 'sa')); ?>
<?php
echo $this->Form->input('title');
echo $this->Form->input('description');
?>
<?php echo $this->Form->end(array('label' => 'submit')); ?>

<?php $this->Paginator->options(array('url' => $this->passedArgs)); ?>

<?php if (!empty($posts)) { ?>
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort("id", "Id"); ?></th>
			<th><?php echo $this->Paginator->sort("title", "Tiêu đề"); ?> </th>
			<th><?php echo $this->Paginator->sort("description", "Nội dung"); ?></th>
		</tr>
		<?php
		foreach ($posts as $item) {
			echo "<tr>";
			echo "<td>" . $item['Book']['id'] . "</td>";
			echo "<td>" . $item['Book']['title'] . "</td>";
			echo "<td>" . $item['Book']['description'] . "</td>";
			echo "</tr>";
		}
		?>
	</table>
	<?php
	echo $this->Paginator->prev('« Previous ', null, null, array('class' => 'disabled'));
	echo " | " . $this->Paginator->numbers() . " | ";
	echo $this->Paginator->next(' Next »', null, null, array('class' => 'disabled'));
	echo ' Page ' . $this->Paginator->counter();
} ?>

