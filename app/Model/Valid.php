<?php
/**
 * Created by PhpStorm.
 * User: sangtx
 * Date: 28/01/2019
 * Time: 10:07
 */

class Valid extends AppModel
{
	public $useTable = false;
	public $validate = array();

	function valid1()
	{
		$this->validate = array(
			"name" => array(
				"rule" => array("lengthBetween", 8, 20),
				"message" => "Name not empty",
			),
			"email" => array(
				"rule" => "notBlank",
				"message" => "Email not empty"
			)
		);

		if ($this->validates($this->validate)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function valid2()
	{
		$this->validate = array(
			'name' => array(
				'dk1' => array(
					'rule' => 'notBlank',
					'message' => 'Name not empty!'
				),
				'dk2' => array(
					'rule' => array('lengthBetween', 6, 10),
					'message' => 'Name between 6 and 10 character'
				)
			),
			'email' => array(
				'dk1' => array(
					'rule' => 'notBlank',
					'message' => 'Email not empty'
				),
				'dk2' => array(
					'rule' => array('email', true),
					'message' => 'Email incorrect format'
				)
			),
			'address' => array(
				'dk1' => array(
					'rule' => 'notBlank',
					'message' => 'Address not empty'
				),
				'dk2' => array(
					'rule' => 'url',
					'message' => 'Address incorrect format'
				)
			)
		);

		if ($this->validates($this->validate)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function valid3()
	{
		$this->validate = array(
			'name' => array(
				'rule' => '/^[a-z0-9]{3,10}$/i',
				'message' => 'Name not incorrect'
			),
			'email' => array(
				'rule' => '/^[a-z A-Z]{1}[a-z A-Z 0-9_]+\@[a-z A-Z 0-9]{2,}\.[a-z A-Z]{2,}$/i',
				'message' => 'Email not incorrect'
			)
		);

		if ($this->validates($this->validate)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function valid4()
	{
		$this->validate = array(
			"username" => array(
				"rule" => 'checkUsername',
				"message" => "Username không chính xác",
			)
		);

		if ($this->validates($this->validate)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function checkUsername()
	{
		if ($this->data['Valid']['username'] == "unitymta") {
			return true;
		} else {
			return false;
		}
	}
}
