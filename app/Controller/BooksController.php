<?php
/**
 * Created by PhpStorm.
 * User: sangtx
 * Date: 25/01/2019
 * Time: 10:58
 */

class BooksController extends AppController
{
	public $name = "Books";
	var $helpers = array("Form", "Paginator", "Html");
	var $paginate = array();

	function index()
	{
		$data = $this->Book->find("all");
		$this->set("data", $data);
	}

	function index2()
	{
		$data = $this->Book->find("all", array("conditions" => array("title LIKE" => "tuYết đoạn hồn"), "limit" => 2));
		$this->set("data", $data);
	}

	function index3()
	{
		$sql = "Select * From books WHERE description LIKE '%dị giới' ";
		$data = $this->Book->query($sql);
		$this->set("data", $data);

	}

	function danhsach()
	{
		$this->paginate = array(
			"limit" => 4,
			"order" => array("id" => "desc"),
			"conditions" => array("Book.id !=" => "11")
		);
		$data = $this->paginate("Book");
		$this->set("data", $data);
	}

	function view()
	{
		$conditions = array();
		$data = array();
		if (!empty($this->passedArgs)) {
			if (isset($this->passedArgs['Book.title'])) {
				$title = $this->passedArgs['Book.title'];
				$conditions[] = array('Book.title LIKE' => "%$title%",);
				$data['Book']['title'] = $title;
			}
			if (isset($this->passedArgs['Book.description'])) {
				$description = $this->passedArgs['Book.description'];
				$conditions[] = array("OR" => array('Book.description LIKE' => "%$description%"));
				$data['Book']['description'] = $description;
			}
			$this->paginate = array('limit' => 5, 'order' => array('id' => 'desc'),);
			$this->data = $data;
			$post = $this->paginate("Book", $conditions);
			$this->set("posts", $post);
		}
	}

	function search()
	{
		$url['action'] = 'view';
		foreach ($this->data as $key => $value) {
			foreach ($value as $key2 => $value2) {
				$url[$key . '.' . $key2] = $value2;
			}
		}
		$this->redirect($url, null, true);
	}
}










