<?php
/**
 * Created by PhpStorm.
 * Users: sangtx
 * Date: 25/01/2019
 * Time: 08:24
 */

class UsersController extends AppController
{
//	var $uses = array("User");
	public $name = "Users"; // name of Controller User
	public $components = array("Cookie", "Session");
	public $helpers = array("Html", "Session");

	function beforeFilter()
	{
		parent::beforeFilter();
	}

	function index()
	{
		$data = $this->User->find("all");
		$this->set(array("data" => $data));
	}


	function login()
	{
		debug($this->request->data);
		if ($this->request->is("post")) {
			if ($this->Auth->login()) {
				if ($this->Auth->user('level') == 1) {
					$this->redirect("/admin/users/index");
				} else {
					$this->redirect("/users/index");
				}
			} else {
				$this->Session->setFlash("Username hoac password sai");
			}
		}
	}

	function info()
	{
		if ($this->Session->check("session")) {
			$username = $this->Session->read("session");
			$this->set("name", $username);
		} else {
			$this->redirect("login");
		}
	}

	function logout()
	{
//		$this->Session->delete("session");
		$this->redirect($this->Auth->logout());
	}


	function admin_index()
	{
//		$data = $this->User->find("all");
//		debug($data);
//		$this->set("data", $data);
//		$data = $this->User->find("all");
//
//		$this->User->bindModel(
//			array('hasOne' => array(
//				'Book' => array(
//					'className' => 'Book'
//				)
//			)
//			)
//		);
		$data = $this->User->find("all");
		$this->set("data", $data);
	}

	function admin_edit($id)
	{
		debug($id);
		debug($this->request->data);
		debug($this->data);
		if (!$id && empty($this->data)) {
			$this->Session->setFlash("User khong phu hop");
			$this->redirect('/admin/users');
		}

		if (empty($this->data)) {
			$this->data = $this->User->read(null, $id);
		} else {
			$this->User->set($this->data);
			if ($this->User->validateUser()) {
				$this->User->save($this->data);
				$this->Session->setFlash("Cap nhat user thanh cong");
				$this->redirect("/admin/users");
			}
		}
	}

	function admin_add()
	{
		if (!empty($this->data)) {
			$this->User->set($this->data);
			if ($this->User->validateUser()) {
				$this->User->save($this->data);
				$this->Session->setFlash("Them user thanh cong", 'default', array('class' => 'success'), 'flash');
				$this->redirect("/admin/users");
			}
		} else {
			$this->render();
		}
	}

	function admin_delete($user_id)
	{
		if (isset($user_id) && is_numeric($user_id)) {
			$data = $this->User->read(null, $user_id);
			if (!empty($data)) {
				$this->User->delete($user_id);
				$this->Session->setFlash("username da duoc xoa voi id=" . $user_id);
			}
		} else {
			$this->Session->setFlash("username khong ton tai");
		}
		$this->redirect("/admin/users");
	}

}

























