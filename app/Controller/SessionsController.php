<?php
/**
 * Created by PhpStorm.
 * User: sangtx
 * Date: 29/01/2019
 * Time: 14:41
 */

class SessionsController extends AppController
{
	public $components = array('Session');

	function index()
	{
		$this->Session->write('Username', 'unityMTA');
		$data = $this->Session->read('Username');
		$this->set('data', $data);
	}
}
